package main

import (
	"cloud-assignment3-slackbot/functionality"
)

//main ...
// For daily update of DB
func main() {
	functionality.MongoDB = functionality.GetADB()
	functionality.MongoDB.Init()
	functionality.MongoDB.AddDailyFix()
}
