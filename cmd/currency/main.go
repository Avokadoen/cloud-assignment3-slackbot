// Author: Aksel Hjerpbakk
// Studentnr: 997816

package main

import (
	"cloud-assignment3-slackbot/functionality"
	"net/http"
	"os"
)

//main ...
// For serving dialogflow and users
func main() {
	functionality.MongoDB = functionality.GetADB()
	functionality.MongoDB.Init()
	port := os.Getenv("PORT")
	http.HandleFunc("/", functionality.URLHandler)
	http.ListenAndServe(":"+port, nil)
}
